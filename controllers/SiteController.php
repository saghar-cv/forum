<?php

namespace app\controllers;

use app\models\SignUpForm;
use app\models\Topic;
use app\models\User;
use Yii;
use yii\base\Exception;
use yii\data\ActiveDataProvider;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;

class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     * @throws Exception
     */
    public function actionIndex()
    {
        if (!$admin = User::findOne(['type' => User::TYPE_ADMIN])) {
            $admin = new User();
            $admin->name = "admin";
            $admin->family = "";
            $admin->username = "admin";
            $admin->password = Yii::$app->getSecurity()->generatePasswordHash("adminadmin");
            $admin->status = User::STATUS_NOT_BLOCKED;
            $admin->type = User::TYPE_ADMIN;
            if (!$admin->save()) {
                Yii::$app->session->setFlash('error', "Couldn't Add Admin.");
            }
        }
        $dataProvider = new ActiveDataProvider([
            'query' => Topic::find()->where(['status' => Topic::STATUS_ACCEPTED])
        ]);
        return $this->render('index', [
            'provider' => $dataProvider
        ]);
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goHome();
        }

        $model->password = '';
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * SingUp action
     *
     * @return string|Response
     * @throws Exception
     */
    public function actionSignup()
    {
        $model = new SignUpForm();
        if ($model->load(Yii::$app->request->post())) {
            if ($user = $model->singUp()) {
                Yii::$app->session->setFlash('success', "You Registered Successfully.");
                return $this->redirect([
                    '/site/index',
                ]);
            }
        }
        return $this->render('signup', [
            'model' => $model
        ]);
    }

    /**
     * Shows all users to Admin
     * @return string
     */
    public function actionUsers()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => User::find()->where(['<>', 'type', User::TYPE_ADMIN])
        ]);
        return $this->render('users', [
            'provider' => $dataProvider
        ]);
    }

    /**
     * Promotes or demotes users
     * @param $id
     * @return Response
     */
    public function actionPromote($id)
    {
        if ($user = User::findOne($id)) {
            if ($user->type == User::TYPE_MODERATOR) {
                $user->type = User::TYPE_USER;
            } else {
                $user->type = User::TYPE_MODERATOR;
            }
            if (!$user->save()) {
                Yii::$app->session->setFlash('error', "Something Went Wrong.");
            }
            Yii::$app->session->setFlash('success', "Success.");
            return $this->redirect([
                'site/users'
            ]);
        } else {
            Yii::$app->session->setFlash('error', "Couldn't Find User.");
            return $this->redirect([
                'site/users'
            ]);
        }
    }

    /**
     * Block or unblock user
     *
     * @param $id
     * @return Response
     */
    public function actionBlock($id)
    {
        if ($user = User::findOne($id)) {
            if ($user->status == User::STATUS_NOT_BLOCKED) {
                $user->status = User::STATUS_BLOCKED;
            } else {
                $user->status = User::STATUS_NOT_BLOCKED;
            }
            if (!$user->save()) {
                Yii::$app->session->setFlash('error', "Something Went Wrong.");
            }
            Yii::$app->session->setFlash('success', "Success.");
            return $this->redirect([
                'site/users'
            ]);
        } else {
            Yii::$app->session->setFlash('error', "Couldn't Find User.");
            return $this->redirect([
                'site/users'
            ]);
        }
    }

}
