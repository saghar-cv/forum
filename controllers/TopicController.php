<?php

namespace app\controllers;

use app\models\Comment;
use app\models\Like;
use app\models\Topic;
use Throwable;
use Yii;
use yii\data\ActiveDataProvider;
use yii\db\StaleObjectException;
use yii\web\Controller;
use yii\web\Response;

class TopicController extends Controller
{

    /**
     * List of Topics, only shown for Admin and Moderator
     *
     * @return string
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Topic::find()
        ]);
        return $this->render('index', [
            'provider' => $dataProvider
        ]);
    }

    /**
     * Create new topic
     *
     * @return string|Response
     */
    public function actionCreate()
    {
        $model = new Topic();
        if ($model->load(Yii::$app->request->post())) {
            $model->status = Topic::STATUS_NOT_ACCEPTED;
            $model->userId = Yii::$app->getUser()->getId();
            if ($model->validate()) {
                if ($model->save()) {
                    Yii::$app->session->setFlash('success', "Topic Created Successfully.");
                } else {
                    Yii::$app->session->setFlash('error', "Something Went Wrong.");
                }
                return $this->redirect([
                    '/site/index',
                ]);
            }
        }
        return $this->render('create', [
            'model' => $model
        ]);
    }

    /**
     * View topic details
     *
     * @param $id
     * @return string
     */
    public function actionView($id)
    {
        if ($topic = Topic::findOne($id)) {
            $comments = Comment::findAll([
                'topicId' => $id,
            ]);
            $model = new Comment();
            return $this->render('view', [
                'topic' => $topic,
                'comments' => $comments,
                'model' => $model
            ]);
        } else {
            Yii::$app->session->setFlash('error', "Topic Not Found.");
            return $this->redirect([
                '/site/index',
            ]);
        }
    }

    /**
     * Change topic status
     *
     * @param $id
     * @param $status
     * @return Response
     */
    public function actionStatus($id, $status)
    {
        if ($topic = Topic::findOne($id)) {
            $topic->status = $status;
            if ($topic->save()) {
                Yii::$app->session->setFlash('success', "Status Changed Successfully.");
            } else {
                Yii::$app->session->setFlash('error', "Something Went Wrong.");
            }
            return $this->redirect([
                '/topic/index',
            ]);
        } else {
            Yii::$app->session->setFlash('error', "Topic Not Found.");
            return $this->redirect([
                '/topic/index',
            ]);
        }
    }

    /**
     * Shows user's topics
     *
     * @return string
     */
    public function actionMy()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Topic::find()->where(['userId' => Yii::$app->user->getId()])
        ]);

        return $this->render('my', [
            'provider' => $dataProvider
        ]);
    }

    /**
     * @param $id
     * @throws Throwable
     * @throws StaleObjectException
     */
    public function actionDelete($id)
    {
        if ($topic = Topic::findOne($id)) {
            if ($topic->delete()) {
                Yii::$app->session->setFlash('success', "Topic Successfully Deleted.");
            } else {
                Yii::$app->session->setFlash('error', "Something Went Wrong.");
            }
        } else {
            Yii::$app->session->setFlash('error', "Topic Not Found.");
        }

        $this->redirect([
            'topic/my'
        ]);
    }

    /**
     * Add new comment
     *
     * @param $topicId
     * @return Response
     */
    public function actionComment($topicId)
    {
        $model = new Comment();
        if ($model->load(Yii::$app->request->post())) {
            $model->userId = Yii::$app->user->getId();
            $model->topicId = $topicId;
            $model->parentId = null;
            if ($model->save()) {
                Yii::$app->session->setFlash('success', "Comment Successfully Added.");
            } else {
                Yii::$app->session->setFlash('error', "Something Went Wrong.");
            }
            return $this->redirect([
                'topic/view',
                'id' => $model->topicId,
            ]);
        }
    }

    /**
     * Like a comment
     *
     * @param $id
     * @param $type
     * @param $topic
     * @param $flag
     * @return Response
     * @throws Throwable
     */
    public function actionLike($id, $type, $topic, $flag)
    {
        if ($flag) {
            $like = new Like();
            $like->userId = Yii::$app->getUser()->getId();
            $like->commentId = $id;
            $like->type = $type;
            if ($like->save()) {
                return $this->redirect([
                    'topic/view',
                    'id' => $topic
                ]);
            }
        } else {
            $like = Like::findOne([
                'userId' => Yii::$app->getUser()->getId(),
                'commentId' => $id,
                'type' => $type
            ]);
            if ($like) {
                if ($like->delete()) {
                    return $this->redirect([
                        'topic/view',
                        'id' => $topic
                    ]);
                }
            }
            return $this->redirect([
                'topic/view',
                'id' => $topic
            ]);
        }
    }

    /**
     * Delete comment
     *
     * @param $id
     * @return Response
     * @throws StaleObjectException
     * @throws Throwable
     */
    public function actionDeleteComment($id)
    {
        if ($comment = Comment::findOne($id)) {
            if ($comment->delete()) {
                Yii::$app->session->setFlash('success', "Comment Successfully Deleted.");
            } else {
                Yii::$app->session->setFlash('error', "Something Went Wrong.");
            }
        } else {
            Yii::$app->session->setFlash('error', "Comment Not Found.");
        }
        return $this->redirect([
            'topic/view',
            'id' => $comment->topicId,
        ]);
    }

    /**
     * Reply to comment
     *
     * @param $id
     * @param $topicId
     * @return Response
     */
    public function actionReply($id, $topicId)
    {
        $model = new Comment();
        if ($model->load(Yii::$app->request->post())) {
            $model->userId = Yii::$app->user->getId();
            $model->topicId = $topicId;
            $model->parentId = $id;
            if ($model->save()) {
                Yii::$app->session->setFlash('success', "Comment Successfully Added.");
            } else {
                Yii::$app->session->setFlash('error', "Something Went Wrong.");
            }
            return $this->redirect([
                'topic/view',
                'id' => $model->topicId,
            ]);
        }
    }

    /**
     * Edit Comment
     *
     * @param $id
     * @return string|Response
     */
    public function actionEditComment($id)
    {
        $model = Comment::findOne($id);
        if ($model->load(Yii::$app->request->post())) {
            if ($model->save()) {
                Yii::$app->session->setFlash('success', "Comment Successfully Edited.");
            } else {
                Yii::$app->session->setFlash('error', "Something Went Wrong.");
            }
            return $this->redirect([
                'topic/view',
                'id' => $model->topicId,
            ]);
        }
        return $this->render('edit', [
            'model' => $model
        ]);
    }


}
