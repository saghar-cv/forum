<?php

use yii\db\Migration;

/**
 * Class m190712_080236_add_reply_to_comment_table
 */
class m190712_080236_add_reply_to_comment_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%comment}}', 'parentId', 'INTEGER AFTER topicId');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%comment}}', 'parentId');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190712_080236_add_reply_to_comment_table cannot be reverted.\n";

        return false;
    }
    */
}
