<?php

use yii\base\NotSupportedException;
use yii\db\Migration;

/**
 * Handles the creation of table `{{%comment}}`.
 */
class m190710_100803_create_comment_table extends Migration
{
    /**
     * {@inheritdoc}
     * @throws NotSupportedException
     */
    public function safeUp()
    {
        $this->createTable('{{%comment}}', [
            'id' => $this->primaryKey(),
            'content' => $this->getDb()->getSchema()->createColumnSchemaBuilder('longtext'),
            'userId' => $this->integer(),
            'topicId' => $this->integer(),
            'createAt' => $this->dateTime(),
            'updateAt' => $this->dateTime(),
        ]);

        $this->addForeignKey(
            'fk_comment_user_id',
            '{{%comment}}',
            'userId',
            '{{%user}}',
            'id',
            'CASCADE',
            'CASCADE'
        );

        $this->addForeignKey(
            'fk_comment_topic_id',
            '{{%comment}}',
            'topicId',
            '{{%topic}}',
            'id',
            'CASCADE',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey(
            'fk_comment_user_id',
            '{{%comment}}'
        );

        $this->dropForeignKey(
            'fk_comment_topic_id',
            '{{%comment}}'
        );

        $this->dropTable('{{%comment}}');
    }
}
