<?php

use yii\db\Migration;

/**
 * Class m190712_085510_add_foreign_key_comment_parent_id
 */
class m190712_085510_add_foreign_key_comment_parent_id extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addForeignKey(
            'fk_comment_parent_id',
            '{{%comment}}',
            'parentId',
            '{{%comment}}',
            'id',
            'CASCADE',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk_comment_parent_id', '{{%comment}}');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190712_085510_add_foreign_key_comment_parent_id cannot be reverted.\n";

        return false;
    }
    */
}
