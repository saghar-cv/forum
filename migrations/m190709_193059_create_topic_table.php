<?php

use yii\base\NotSupportedException;
use yii\db\Migration;

/**
 * Handles the creation of table `{{%topic}}`.
 */
class m190709_193059_create_topic_table extends Migration
{
    /**
     * {@inheritdoc}
     * @throws NotSupportedException
     */
    public function safeUp()
    {
        $this->createTable('{{%topic}}', [
            'id' => $this->primaryKey(),
            'title' => $this->string(),
            'userId' => $this->integer()->notNull(),
            'content' => $this->getDb()->getSchema()->createColumnSchemaBuilder('longtext'),
            'status' => $this->tinyInteger(),
            'createAt' => $this->dateTime(),
            'updateAt' => $this->dateTime(),
        ]);

        $this->addForeignKey(
            'fk_topic_user_id',
            '{{%topic}}',
            'userId',
            '{{%user}}',
            'id',
            'CASCADE',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey(
            'fk_topic_user_id',
            'topic'
        );

        $this->dropTable('{{%topic}}');
    }
}
