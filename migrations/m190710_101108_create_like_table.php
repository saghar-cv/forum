<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%like}}`.
 */
class m190710_101108_create_like_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%like}}', [
            'id' => $this->primaryKey(),
            'type' => $this->tinyInteger(),
            'commentId' => $this->integer(),
            'userId' => $this->integer(),
        ]);

        $this->addForeignKey(
            'fk_like_comment_id',
            '{{%like}}',
            'commentId',
            '{{%comment}}',
            'id',
            'CASCADE',
            'CASCADE'
        );

        $this->addForeignKey(
            'fk_like_user_id',
            '{{%like}}',
            'userId',
            '{{%user}}',
            'id',
            'CASCADE',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey(
            'fk_like_comment_id',
            '{{%like}}'
        );

        $this->dropForeignKey(
            'fk_like_user_id',
            '{{%like}}'
        );

        $this->dropTable('{{%like}}');
    }
}
