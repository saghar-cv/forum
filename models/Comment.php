<?php

namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;
use yii\db\Expression;

/**
 * This is the model class for table "{{%comment}}".
 *
 * @property int $id
 * @property string $content
 * @property int $userId
 * @property int $topicId
 * @property int $parentId
 * @property string $createAt
 * @property string $updateAt
 *
 * @property Topic $topic
 * @property User $user
 * @property Like[] $likes
 * @property Comment[] $comments
 */
class Comment extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%comment}}';
    }

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'createAt',
                'updatedAtAttribute' => 'updateAt',
                'value' => new Expression('NOW()'),
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['content'], 'required'],
            [['userId', 'topicId', 'parentId'], 'integer'],
            [['createAt', 'updateAt'], 'safe'],
            [['content'], 'string', 'max' => 255],
            [['topicId'], 'exist', 'skipOnError' => true, 'targetClass' => Topic::className(), 'targetAttribute' => ['topicId' => 'id']],
            [['userId'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['userId' => 'id']],
            [['parentId'], 'exist', 'skipOnError' => true, 'targetClass' => Comment::className(), 'targetAttribute' => ['parentId' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'content' => 'Content',
            'userId' => 'User ID',
            'topicId' => 'Topic ID',
            'parentId' => 'Parent ID',
            'createAt' => 'Create At',
            'updateAt' => 'Update At',
        ];
    }

    /**
     * @return ActiveQuery
     */
    public function getTopic()
    {
        return $this->hasOne(Topic::className(), ['id' => 'topicId']);
    }

    /**
     * @return ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'userId']);
    }

    /**
     * @return ActiveQuery
     */
    public function getLikes()
    {
        return $this->hasMany(Like::className(), ['commentId' => 'id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getParent()
    {
        return $this->hasOne(Comment::className(), ['id' => 'parentId']);
    }

    /**
     * @return ActiveQuery
     */
    public function getComments()
    {
        return $this->hasMany(Comment::className(), ['parentId' => 'id']);
    }

    /**
     * Get like count
     *
     * @return int
     */
    public function getLikeCount($type)
    {
        $count = 0;
        foreach ($this->likes as $like) {
            if ($like->type == $type)
                $count++;
        }
        return $count;
    }

    /**
     * Checks if user liked or disliked a comment
     *
     * @param $type
     * @return bool
     */
    public function userLiked($type)
    {
        $like = Like::findOne([
            'userId' => Yii::$app->user->id,
            'type' => $type,
            'commentId' => $this->id
        ]);

        if ($like) {
            return true;
        }

        return false;
    }
}
