<?php

namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;
use yii\db\Expression;
use yii\web\IdentityInterface;

/**
 * This is the model class for table "{{%user}}".
 *
 * @property int $id
 * @property string $username
 * @property string $name
 * @property string $family
 * @property int $type
 * @property string $password
 * @property int $status
 * @property string $createAt
 * @property string $updateAt
 *
 * @property Comment[] $comments
 * @property Like[] $likes
 * @property Topic[] $topics
 */
class User extends ActiveRecord implements IdentityInterface
{
    const TYPE_ADMIN = 0;
    const TYPE_MODERATOR = 1;
    const TYPE_USER = 2;

    const STATUS_NOT_BLOCKED = 0;
    const STATUS_BLOCKED = 1;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%user}}';
    }

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'createAt',
                'updatedAtAttribute' => 'updateAt',
                'value' => new Expression('NOW()'),
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['username'], 'required'],
            [['type', 'status'], 'integer'],
            [['createAt', 'updateAt'], 'safe'],
            [['username', 'password'], 'string', 'max' => 255],
            [['name', 'family'], 'string', 'max' => 259],
            [['username'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'username' => 'Username',
            'name' => 'Name',
            'family' => 'Family',
            'type' => 'Type',
            'password' => 'Password',
            'status' => 'Status',
            'createAt' => 'Create At',
            'updateAt' => 'Update At',
        ];
    }

    /**
     * @return ActiveQuery
     */
    public function getComments()
    {
        return $this->hasMany(Comment::className(), ['userId' => 'id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getLikes()
    {
        return $this->hasMany(Like::className(), ['userId' => 'id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getTopics()
    {
        return $this->hasMany(Topic::className(), ['userId' => 'id']);
    }

    /**
     * @param $username
     * @return User|bool|null
     */
    public static function getByUsername($username)
    {
        $user = User::findOne($username);
        if ($user) {
            return $user;
        }
        return false;
    }

    /**
     * Checks if user is admin
     *
     * @param $userId
     * @return bool
     */
    public static function isAdmin($userId)
    {
        if ($user = User::findOne($userId)) {
            if ($user->type == User::TYPE_ADMIN)
                return true;

        }
        return false;
    }

    /**
     * Checks if user is moderator
     *
     * @param $userId
     * @return bool
     */
    public static function isModerator($userId)
    {
        if ($user = User::findOne($userId)) {
            if ($user->type == User::TYPE_MODERATOR)
                return true;

        }
        return false;
    }

    /**
     * Checks if user is a normal user
     *
     * @param $userId
     * @return bool
     */
    public static function isUser($userId)
    {
        if ($user = User::findOne($userId)) {
            if ($user->type == User::TYPE_USER)
                return true;

        }
        return false;
    }

    /**
     * Promote user to moderator
     *
     * @param $userId
     * @return bool
     */
    public function promoteUser($userId)
    {
        if ($this->isAdmin(Yii::$app->getUser()->getId())) {
            if ($user = User::findOne($userId)) {
                $user->type = User::TYPE_MODERATOR;
                return $user->save();
            }
        }
        return false;
    }

    /**
     * Finds an identity by the given ID.
     * @param string|int $id the ID to be looked for
     * @return IdentityInterface the identity object that matches the given ID.
     * Null should be returned if such an identity cannot be found
     * or the identity is not in an active state (disabled, deleted, etc.)
     */
    public static function findIdentity($id)
    {
        return static::findOne($id);
    }

    /**
     * Finds an identity by the given token.
     * @param mixed $token the token to be looked for
     * @param mixed $type the type of the token. The value of this parameter depends on the implementation.
     * For example, [[\yii\filters\auth\HttpBearerAuth]] will set this parameter to be `yii\filters\auth\HttpBearerAuth`.
     * @return IdentityInterface the identity object that matches the given token.
     * Null should be returned if such an identity cannot be found
     * or the identity is not in an active state (disabled, deleted, etc.)
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        return static::findOne(['access_token' => $token]);
    }

    /**
     * Returns an ID that can uniquely identify a user identity.
     * @return string|int an ID that uniquely identifies a user identity.
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Returns a key that can be used to check the validity of a given identity ID.
     *
     * The key should be unique for each individual user, and should be persistent
     * so that it can be used to check the validity of the user identity.
     *
     * The space of such keys should be big enough to defeat potential identity attacks.
     *
     * This is required if [[User::enableAutoLogin]] is enabled. The returned key will be stored on the
     * client side as a cookie and will be used to authenticate user even if PHP session has been expired.
     *
     * Make sure to invalidate earlier issued authKeys when you implement force user logout, password change and
     * other scenarios, that require forceful access revocation for old sessions.
     *
     * @return string a key that is used to check the validity of a given identity ID.
     * @see validateAuthKey()
     */
    public function getAuthKey()
    {
        // TODO: Implement getAuthKey() method.
    }

    /**
     * Validates the given auth key.
     *
     * This is required if [[User::enableAutoLogin]] is enabled.
     * @param string $authKey the given auth key
     * @return bool whether the given auth key is valid.
     * @see getAuthKey()
     */
    public function validateAuthKey($authKey)
    {
        // TODO: Implement validateAuthKey() method.
    }
}
