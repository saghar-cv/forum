<?php


namespace app\models;


use Yii;
use yii\base\Exception;
use yii\base\Model;

class SignUpForm extends Model
{
    public $username;
    public $name;
    public $family;
    public $password;
    public $type;
    public $status;

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['username', 'password'], 'required'],
            [['username'], 'validateUsername'],
            ['name', 'string'],
            ['family', 'string'],
        ];
    }

    /**
     * Validate username
     * @return bool
     */
    public function validateUsername()
    {
        if (User::findAll(['username' => $this->username]))
            return false;

        return true;
    }

    /**
     * @return null |null
     * @throws Exception
     */
    public function singUp()
    {
        if (!$this->validate()) {
            return null;
        }

        $user = new User();
        $user->name = $this->name;
        $user->family = $this->family;
        $user->username = $this->username;
        $user->password = Yii::$app->getSecurity()->generatePasswordHash($this->password);
        $user->status = User::STATUS_NOT_BLOCKED;
        $user->type = User::TYPE_USER;

        if ($user->save()) {
            return Yii::$app->user->login($user, 3600 * 24 * 30);
        } else {
            Yii::error($user->getErrors(), self::className());
            $this->addErrors($user->getErrors());
            return false;
        }

    }
}