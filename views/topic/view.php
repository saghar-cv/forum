<?php
/* @var $this View */
/* @var $topic Topic */
/* @var $comments Comment[] */

/* @var $model Comment */

use app\models\Comment;
use app\models\Like;
use app\models\Topic;
use app\models\User;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;

$this->title = $topic->title;
$this->params['breadcrumbs'][] = $this->title;

$userId = Yii::$app->getUser()->id;
?>


<div class="view-topic">
    <h1 style="margin-bottom: 20px"><?= Html::encode($this->title) ?></h1>
    <p style="border: 1px #d0f3ff; background: #e9f4ff; font-size: medium; margin-bottom: 20px;"><?= $topic->content ?></p>
    <div class="form-group">
        <div class="col-md-4"
             style="font-size: small; margin-bottom: 20px"><?= $topic->user->name . ' ' . $topic->user->family ?></div>
        <? if ($topic->userId == Yii::$app->getUser()->getId()): ?>
            <? if ($topic->status == Topic::STATUS_NOT_ACCEPTED): ?>
                <div class="col-md-4" style="font-size: small">Status: Pending</div>
            <? elseif ($topic->status == Topic::STATUS_ACCEPTED): ?>
                <div class="col-md-4" style="font-size: small">Status: Accepted</div>
            <? elseif ($topic->status == Topic::STATUS_REJECTED): ?>
                <div class="col-md-4" style="font-size: small">Status: Rejected</div>
            <? endif; ?>
        <? endif; ?>
        <? if ($topic->status == Topic::STATUS_CLOSED): ?>
            <div class="col-md-4" style="font-size: small">Status: Closed</div>
        <? endif; ?>
        <div class="col-md-4" style="font-size: small">Created At: <?= $topic->createAt ?></div>
    </div>

    <? if (!Yii::$app->getUser()->isGuest): ?>
        <div>
            <?php $form = ActiveForm::begin(['action' => Url::to(['topic/comment', 'topicId' => $topic->id]), 'options' => ['method' => 'post']]) ?>
            <?= $form->field($model, 'content')->textInput(['placeholder' => "Enter Your Comment"])->label(false) ?>
            <div class="form-group">
                <?= Html::submitButton('Submit', ['class' => 'btn btn-primary', 'name' => 'comment-button']) ?>
            </div>

            <?php ActiveForm::end(); ?>
        </div>
    <? endif; ?>

    <? if ($comments != null): ?>
        <? foreach ($comments as $comment): ?>
            <div style="font-size: medium; margin-bottom: 10px; padding: 8px; border: 1px solid gray; border-radius: 5px;">
                <h4><?= $comment->user->name ?> :</h4>
                <h5><?= $comment->user->username ?></h5>
                <p>
                    <?= $comment->content ?>
                </p>
                <p style="font-size: xx-small">
                    <? if (!$comment->parentId == null): ?>
                        Replied to comment <?= $comment->parentId ?>
                    <? endif; ?>
                    Comment id: <?= $comment->id ?>
                </p>
            </div>
            <? if ($comment->userLiked(Like::TYPE_LIKE)): ?>
                <?= Html::a('Unlike', ['topic/like', 'id' => $comment->id, 'type' => Like::TYPE_LIKE, 'topic' => $topic->id, 'flag' => false]); ?>
            <? else: ?>
                <?= Html::a('Like', ['topic/like', 'id' => $comment->id, 'type' => Like::TYPE_LIKE, 'topic' => $topic->id, 'flag' => true]); ?>
            <? endif; ?>
            <?= $comment->getLikeCount(Like::TYPE_LIKE) ?>
            <? if ($comment->userLiked(Like::TYPE_DISLIKE)): ?>
                <?= Html::a('UnDislike', ['topic/like', 'id' => $comment->id, 'type' => Like::TYPE_DISLIKE, 'topic' => $topic->id, 'flag' => false]); ?>
            <? else: ?>
                <?= Html::a('Dislike', ['topic/like', 'id' => $comment->id, 'type' => Like::TYPE_DISLIKE, 'topic' => $topic->id, 'flag' => true]); ?>
            <? endif; ?>
            <?= $comment->getLikeCount(Like::TYPE_DISLIKE) ?>
            <? if (User::isAdmin($userId) or User::isModerator($userId) or $comment->userId == $userId): ?>
                <?= Html::a('Delete', ['topic/delete-comment', 'id' => $comment->id]); ?>
            <? endif; ?>
            <? if ($comment->userId == $userId): ?>
                <?= Html::a('Edit', ['topic/edit-comment', 'id' => $comment->id]); ?>
            <? endif; ?>
            <div style="margin-left: 20px;margin-top: 10px;">
                <?php $form = ActiveForm::begin(['action' => Url::to(['topic/reply', 'id' => $comment->id, 'topicId' => $topic->id]), 'options' => ['method' => 'post']]) ?>
                <?= $form->field($model, 'content')->textInput(['placeholder' => "Enter Your Comment"])->label(false) ?>
                <div class="form-group">
                    <?= Html::submitButton('Reply', ['class' => 'btn btn-primary', 'name' => 'comment-button']) ?>
                </div>

                <?php ActiveForm::end(); ?>
            </div>

        <? endforeach; ?>
    <? endif; ?>


</div>
