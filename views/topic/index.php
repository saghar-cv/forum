<?php
/* @var $this yii\web\View */
/* @var $provider ActiveDataProvider */
$this->title = 'Topics';
$this->params['breadcrumbs'][] = $this->title;

use app\models\Topic;
use yii\data\ActiveDataProvider;
use yii\grid\GridView;
use yii\helpers\Html; ?>

    <h1><?= Html::encode($this->title) ?></h1>

<?= GridView::widget([
    'dataProvider' => $provider,
    'columns' => [
        ['class' => 'yii\grid\SerialColumn'],
        'title',
        'content',
        [
            'attribute' => 'userId',
            'label' => 'User',
            'value' => function ($data) {
                return $data->user->name . ' ' . $data->user->family;
            },

        ],
        [
            'attribute' => 'status',
            'value' => function ($data) {
                switch ($data->status) {
                    case Topic::STATUS_NOT_ACCEPTED:
                        return 'Pending';
                    case Topic::STATUS_ACCEPTED:
                        return 'Accepted';
                    case Topic::STATUS_REJECTED:
                        return 'Rejected';
                    case Topic::STATUS_CLOSED:
                        return 'Closed';
                }
            },

        ],
        'createAt:datetime',
        'updateAt:datetime',
        [
            'label' => 'Change Status',
            'format' => 'raw',
            'value' => function ($data) {
                if ($data->status == Topic::STATUS_NOT_ACCEPTED)
                    return Html::a('Accept', ['topic/status', 'id' => $data->id, 'status' => Topic::STATUS_ACCEPTED]) . ' / ' .
                        Html::a('Reject', ['topic/status', 'id' => $data->id, 'status' => Topic::STATUS_REJECTED]);
                if ($data->status == Topic::STATUS_ACCEPTED)
                    return Html::a('Close', ['topic/status', 'id' => $data->id, 'status' => Topic::STATUS_CLOSED]);
                if ($data->status == Topic::STATUS_REJECTED)
                    return Html::a('Accept', ['topic/status', 'id' => $data->id, 'status' => Topic::STATUS_ACCEPTED]);
                if ($data->status == Topic::STATUS_CLOSED)
                    return Html::a('Open', ['topic/status', 'id' => $data->id, 'status' => Topic::STATUS_ACCEPTED]);
            },
        ],
        [
            'class' => 'yii\grid\ActionColumn',
            'header' => 'Actions',
            'headerOptions' => ['style' => 'color:#337ab7'],
            'template' => '{view}',
            'buttons' => [
                'view' => function ($url, $model) {
                    return Html::a('<span class="glyphicon glyphicon-eye-open"></span>', $url, [
                        'title' => Yii::t('app', 'view'),
                    ]);
                },

            ],
            'urlCreator' => function ($action, $model, $key, $index) {
                if ($action === 'view') {
                    $url = 'view?id=' . $model->id;
                    return $url;
                }
            }
        ],
    ],
]);
?>