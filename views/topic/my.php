<?php
/* @var $this yii\web\View */
/* @var $provider ActiveDataProvider */
$this->title = 'My Topics';
$this->params['breadcrumbs'][] = $this->title;

use app\models\Topic;
use yii\data\ActiveDataProvider;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\web\View; ?>

    <h1><?= Html::encode($this->title) ?></h1>

<?= GridView::widget([
    'dataProvider' => $provider,
    'columns' => [
        ['class' => 'yii\grid\SerialColumn'],
        'title',
        'content',
        [
            'attribute' => 'userId',
            'label' => 'User',
            'value' => function ($data) {
                return $data->user->name . ' ' . $data->user->family;
            },

        ],
        [
            'attribute' => 'status',
            'value' => function ($data) {
                switch ($data->status) {
                    case Topic::STATUS_NOT_ACCEPTED:
                        return 'Pending';
                    case Topic::STATUS_ACCEPTED:
                        return 'Accepted';
                    case Topic::STATUS_REJECTED:
                        return 'Rejected';
                    case Topic::STATUS_CLOSED:
                        return 'Closed';
                }
            },

        ],
        'createAt:datetime',
        'updateAt:datetime',
        [
            'class' => 'yii\grid\ActionColumn',
            'header' => 'Actions',
            'headerOptions' => ['style' => 'color:#337ab7'],
            'template' => '{view}{delete}',
            'buttons' => [
                'view' => function ($url, $model) {
                    return Html::a('<span class="glyphicon glyphicon-eye-open"></span>', $url, [
                        'title' => Yii::t('app', 'view'),
                    ]);
                },
                'delete' => function ($url, $model) {
                    return Html::a('<span class="glyphicon glyphicon-trash"></span>', $url, [
                        'title' => Yii::t('app', 'delete'),
                    ]);
                }

            ],
            'urlCreator' => function ($action, $model, $key, $index) {
                if ($action === 'view') {
                    $url = 'view?id=' . $model->id;
                    return $url;
                }
                if ($action === 'delete') {
                    $url ='delete?id='.$model->id;
                    return $url;
                }
            }
        ],
    ],
]);
?>