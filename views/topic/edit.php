<?php

/* @var $this View */

/* @var $model Comment */

use app\models\Comment;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;

$this->title = "Edit Comment";
$this->params['breadcrumbs'][] = $this->title;
?>

<div>
    <?php $form = ActiveForm::begin(['action' => Url::to(['topic/edit-comment', 'id' => $model->id]), 'options' => ['method' => 'post']]) ?>
    <?= $form->field($model, 'content')->textInput(['placeholder' => "Enter Your Comment"])->label(false) ?>
    <div class="form-group">
        <?= Html::submitButton('Submit', ['class' => 'btn btn-primary', 'name' => 'comment-button']) ?>
    </div>

    <?php ActiveForm::end(); ?>
</div>
