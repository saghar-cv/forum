<?php
/* @var $model Topic */

use app\models\Topic;
use yii\helpers\Html;

?>

<div class="list-item col-sm-12" data-key="<?= $model['id'] ?>" style="border:1px solid gray; margin-bottom: 15px; border-radius: 10px;">
    <h3><?= Html::a(Html::encode($model['title']), ['topic/view', 'id' => $model->id]); ?></h3>
    <p><?= Html::encode($model['content']); ?></p>
</div>