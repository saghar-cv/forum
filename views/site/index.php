<?php

/* @var $this yii\web\View */
/* @var $provider ActiveDataProvider */
/* @var $comment Comment */

$this->title = 'Forum';

use app\models\Comment;
use yii\data\ActiveDataProvider;
use yii\helpers\Html;
use yii\widgets\ListView; ?>
<div class="site-index">

    <div class="jumbotron">
        <h1>Forum</h1>

        <p class="lead">Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>

        <? if (!Yii::$app->getUser()->isGuest): ?>
            <p><?= Html::a('Create new Topic', ['topic/create'], ['class' => 'btn btn-lg btn-success']) ?></p>
        <? endif; ?>
    </div>

    <div>
        <?=
        ListView::widget([
            'dataProvider' => $provider,
            'options' => [
                'tag' => 'div',
                'class' => 'bordered bgblack',
                'id' => 'list-wrapper',
            ],
            'layout' => "{pager}\n{items}\n{summary}",
            'itemView' => function ($model, $key, $index, $widget) {
                $itemContent = $this->render('_list_item', ['model' => $model]);
                return $itemContent;
            },
            'pager' => [
                'firstPageLabel' => 'First',
                'lastPageLabel' => 'Last',
                'maxButtonCount' => 4,
                'options' => [
                    'class' => 'pagination col-xs-12'
                ]
            ]
        ]);
        ?>
    </div>