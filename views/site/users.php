<?php
/* @var $this View */

/* @var $provider ActiveDataProvider */

use app\models\User;
use yii\data\ActiveDataProvider;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\web\View;

$this->title = 'Users';
$this->params['breadcrumbs'][] = $this->title;
$user = User::findOne(Yii::$app->user->getId());
?>

<h1><?= Html::encode($this->title) ?></h1>
<? if ($user): ?>
    <? if ($user->type == User::TYPE_ADMIN): ?>
        <?= GridView::widget([
            'dataProvider' => $provider,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],
                'name',
                'family',
                'username',
                [
                    'attribute' => 'type',
                    'value' => function ($data) {
                        if ($data->type == User::TYPE_USER)
                            return 'User';
                        return 'Moderator';
                    },

                ],
                'createAt:datetime',
                'updateAt:datetime',
                [
                    'label' => 'Promotion',
                    'format' => 'raw',
                    'value' => function ($data) {
                        if ($data->type == User::TYPE_MODERATOR)
                            return Html::a('Demote', ['site/promote', 'id' => $data->id]);
                        return Html::a('Promote', ['site/promote', 'id' => $data->id]);
                    },
                ],
                [
                    'label' => 'Block',
                    'format' => 'raw',
                    'value' => function ($data) {
                        if ($data->status == User::STATUS_NOT_BLOCKED)
                            return Html::a('Block', ['site/block', 'id' => $data->id]);
                        return Html::a('UnBlock', ['site/block', 'id' => $data->id]);
                    },
                ],
            ],
        ]);
        ?>
    <? elseif ($user->type == User::TYPE_MODERATOR): ?>
        <?= GridView::widget([
            'dataProvider' => $provider,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],
                'name',
                'family',
                'username',
                [
                    'attribute' => 'type',
                    'value' => function ($data) {
                        if ($data->type == User::TYPE_USER)
                            return 'User';
                        return 'Moderator';
                    },

                ],
                'createAt:datetime',
                'updateAt:datetime',
                [
                    'label' => 'Block',
                    'format' => 'raw',
                    'value' => function ($data) {
                        if ($data->status == User::STATUS_NOT_BLOCKED)
                            return Html::a('Block', ['site/block', 'id' => $data->id]);
                        return Html::a('UnBlock', ['site/block', 'id' => $data->id]);
                    },
                ],
            ],
        ]);
        ?>
    <? endif; ?>
<? endif; ?>
